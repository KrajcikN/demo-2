<?php

namespace app\application\entities;

/**
 * This is the model class for table "{{%bonuses}}".
 *
 * @property integer $id
 * @property string  $name
 * @property integer $min
 * @property integer $max
 * @property integer $cost
 * @property integer $active
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bonuses}}';
    }

    public function apply(int $count, int $sum): int
    {
        if ($this->isActive($count, $sum)) {
            return ($count - $this->checkStepBoundaries($count, $sum)) * $this->cost;
        }

        return 0;
    }

    /**
     * @param int $count
     * @param int $sum
     *
     * @return bool
     */
    private function isActive(int $count, int $sum): bool
    {
        return $sum >= $this->min && (null === $this->max || ($sum - $count) <= $this->max);
    }

    /**
     * @param int $count
     * @param int $sum
     *
     * @return int
     */
    private function checkStepBoundaries(int $count, int $sum): int
    {
        $startOfInterval = $sum - $count;
        $reducer = 0;
        if (($startOfInterval) < $this->min && $sum >= $this->min) {
            $reducer += $this->min - ($startOfInterval) - 1;
        }
        if (null !== $this->max && $sum > $this->max && ($startOfInterval) <= $this->max) {
            $reducer += $sum - $this->max;
        }

        return $reducer;
    }
}
