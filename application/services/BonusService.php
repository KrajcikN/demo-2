<?php

namespace app\application\services;


use app\application\dto\EmployeeBonusHistoryDto;
use app\application\entities\date\Month;
use app\application\entities\Employee;
use app\application\repositories\BonusRepository;
use app\application\repositories\EmployeeReadRepository;

class BonusService
{
    /**
     * @var BonusRepository
     */
    private $bonusRepository;
    /**
     * @var EmployeeReadRepository
     */
    private $employees;

    /**
     * BonusService constructor.
     *
     * @param BonusRepository        $bonusRepository
     * @param EmployeeReadRepository $employees
     */
    public function __construct(BonusRepository $bonusRepository, EmployeeReadRepository $employees)
    {
        $this->bonusRepository = $bonusRepository;
        $this->employees = $employees;
    }


    /**
     * @param Employee $employee
     * @param Month    $month
     *
     * @return EmployeeBonusHistoryDto
     * @throws \DomainException
     */
    public function getMonthlyByEmployee(Employee $employee, Month $month): EmployeeBonusHistoryDto
    {
        if (isset($employee->bonuses[$month->toString()])) {
            return $employee->bonuses[$month->toString()];
        }
        $calls = $this->employees->getCallsHistoryByMont($employee, $month);
        $bonuses = $this->bonusRepository->getActive();
        $sum = 0;
        $result = [];
        foreach ($calls as $date => $callsCount) {
            $sum += $callsCount;
            foreach ($bonuses as $bonus) {
                $result[$date][$bonus->id] = $bonus->apply($callsCount, $sum);
            }
        }
        $employee->bonuses[$month->toString()] = $result;

        return new EmployeeBonusHistoryDto(
            $employee->id,
            $employee->name,
            $result,
            $month
        );
    }
}