<?php

namespace app\application\dto;


use app\application\entities\date\Month;

class EmployeeBonusHistoryDto
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var Month
     */
    private $month;
    /**
     * @var array
     */
    private $bonuses;

    /**
     * EmployeeSalaryDto constructor.
     *
     * @param int    $id
     * @param string $name
     * @param array  $bonuses
     * @param Month  $month
     *
     */
    public function __construct(
        int $id,
        string $name,
        array $bonuses,
        Month $month
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->bonuses = $bonuses;
        $this->month = $month;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getBonuses(): array
    {
        return $this->bonuses;
    }

    /**
     * @return Month
     */
    public function getMonth(): Month
    {
        return $this->month;
    }

}