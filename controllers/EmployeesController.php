<?php

namespace app\controllers;


use app\application\entities\date\Month;
use app\application\repositories\EmployeeReadRepository;
use app\application\services\BonusService;
use app\application\services\SalaryCalculationService;
use yii\base\Module;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class EmployeesController extends Controller
{
    /**
     * @var BonusService
     */
    private $bonusService;
    /**
     * @var SalaryCalculationService
     */
    private $salaryCalculationService;

    /**
     * @inheritDoc
     */
    public function __construct(
        $id,
        Module $module,
        BonusService $bonusService,
        SalaryCalculationService $salaryCalculationService,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->bonusService = $bonusService;
        $this->salaryCalculationService = $salaryCalculationService;
    }

    public function actionIndex()
    {
        try {
            $month = new Month('2015', '01');
            $employees = EmployeeReadRepository::getAll();
            $result = [];
            foreach ($employees as $employee) {
                $result[] = $this->salaryCalculationService->calculateMonthlyByEmployee($employee, $month);
            }
        } catch (\DomainException $exception) {
            \Yii::$app->session->setFlash('error', nl2br($exception->getMessage()));
            \Yii::$app->errorHandler->logException($exception);

            return $this->goBack();
        }

        return $this->render(
            'index',
            [
                'employees' => $result,
            ]
        );
    }

    public function actionBonusHistory(int $id)
    {
        $employee = EmployeeReadRepository::find($id);
        if (null === $employee) {
            throw new NotFoundHttpException('Employee not found');
        }
        try {
            $month = new Month('2015', '01');
            $dto = $this->bonusService->getMonthlyByEmployee($employee, $month);
        } catch (\DomainException $exception) {
            \Yii::$app->session->setFlash('error', nl2br($exception->getMessage()));
            \Yii::$app->errorHandler->logException($exception);

            return $this->goBack();
        }

        return $this->render(
            'bonus-history',
            [
                'bonuses' => $dto,
            ]
        );
    }
}