<?php

namespace app\application\repositories;

use app\application\entities\CallingHistory;
use app\application\entities\date\Month;
use app\application\entities\Employee;

class EmployeeReadRepository
{
    /**
     * @param int $id
     *
     * @return Employee|null
     */
    public static function find(int $id): ?Employee
    {
        return Employee::find()->andWhere(['id' => $id])->limit(1)->one();
    }

    /**
     * @param string $name
     *
     * @return Employee|null
     */
    public static function findByName(string $name): ?Employee
    {
        return Employee::find()->andWhere(
            [
                "LOWER(username) = LOWER(:name)",
                ['name' => $name],
            ]
        )->limit(1)->one();
    }

    /**
     * @return Employee[]
     */
    public static function getAll(): array
    {
        return Employee::find()->all();
    }

    /**
     * @param Employee $employee
     * @param Month    $month
     *
     * @return array
     * @throws \DomainException
     */
    public function getCallsHistoryByMont(Employee $employee, Month $month): array
    {
        $tableName = CallingHistory::tableName();

        $calls = $employee
            ->hasMany(CallingHistory::class, ['user_id' => 'id'])
            ->select(['calls'])
            ->indexBy('date')
            ->andWhere(['working_status' => CallingHistory::STATUS_WORKING])
            ->andWhere(['>', 'calls', 0])
            ->andWhere("strftime('%Y-%m', {$tableName}.date) = :date", ['date' => $month->toString()])
            ->orderBy(['date' => SORT_ASC])
            ->column();

        return $calls;
    }
}