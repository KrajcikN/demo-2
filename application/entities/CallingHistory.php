<?php

namespace app\application\entities;

/**
 * This is the model class for table "{{%call_handling}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string  $date
 * @property integer $calls
 * @property integer $working_status
 */
class CallingHistory extends \yii\db\ActiveRecord
{
    const STATUS_WORKING = 1;
    const STATUS_DAY_OFF = 2;
    const STATUS_NOT_WORKING = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call_handling}}';
    }
}
