<?php

namespace app\application\entities\date;

class Month
{
    const FORMAT = 'Y-m';
    /**
     * @var bool|\DateTimeImmutable
     */
    public $date;

    /**
     * Month constructor.
     *
     * @param string $year  Y
     * @param string $month m
     *
     * @throws \RuntimeException
     */
    public function __construct(string $year, string $month)
    {
        $this->date = \DateTimeImmutable::createFromFormat(self::FORMAT, "$year-$month");
        if (false === $this->date) {
            throw new \RuntimeException('Invalid date format, expect '.self::FORMAT);
        }
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->date->format(self::FORMAT);
    }
}