<?php
//\yii\helpers\VarDumper::dump(getenv('DATABASE_DSN'));die;
//\yii\helpers\VarDumper::dump(scandir('./data/sqlite/yii2.db'));die;
return [
    'class' => 'yii\db\Connection',
    'dsn' => getenv('DATABASE_DSN'),
    'username' => getenv('DATABASE_USER'),
    'password' => getenv('DATABASE_PASSWORD'),
    'charset' => 'utf8',
    'tablePrefix' => getenv('DATABASE_TABLE_PREFIX'),
    'enableSchemaCache' => YII_ENV_PROD ? true : false,

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];