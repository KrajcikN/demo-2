<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

/* @var $employees \app\application\dto\EmployeeSalaryDto[] */

$this->title = 'Employees salary';
?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="employees-index">
    <ul>
        <?php foreach ($employees as $employee) {
            echo '<li>'.Html::a(
                    Html::encode($employee->getName()),
                    ['employees/bonus-history', 'id' => $employee->getId()]
                ).': '.Yii::$app->formatter->asCurrency(
                    $employee->getSalaryForHumans()
                ).'</li>';
        } ?>
    </ul>
</div>
