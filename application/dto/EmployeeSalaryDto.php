<?php

namespace app\application\dto;


use app\application\entities\date\Month;
use app\application\helpers\MoneyHelper;

class EmployeeSalaryDto
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $salary;
    /**
     * @var Month
     */
    private $month;

    /**
     * EmployeeSalaryDto constructor.
     *
     * @param int    $id
     * @param string $name
     * @param int    $salary
     * @param Month  $month
     */
    public function __construct(
        int $id,
        string $name,
        int $salary,
        Month $month
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->salary = $salary;
        $this->month = $month;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }

    /**
     * @return int
     */
    public function getSalaryForHumans(): int
    {
        return MoneyHelper::moneyForHumans($this->salary);
    }

    /**
     * @return Month
     */
    public function getMonth(): Month
    {
        return $this->month;
    }
}