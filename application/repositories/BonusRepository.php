<?php

namespace app\application\repositories;


use app\application\entities\Bonus;

class BonusRepository
{
    /**
     * @return Bonus[]
     * @throws \DomainException
     */
    public function getActive(): array
    {
        if (1 < $count = Bonus::find()->andWhere(['active' => 1])->andWhere(['max' => null])->count()) {
            throw new \DomainException('There can be only one bonus without an upper limit, found '.$count);
        }

        $bonuses = Bonus::find()->andWhere(['active' => 1])->all();
        if ([] === $bonuses) {
            throw new \DomainException('Not Found bonuses');
        }

        return $bonuses;
    }
}