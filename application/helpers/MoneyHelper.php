<?php

namespace app\application\helpers;


class MoneyHelper
{
    /**
     * @param int $balance
     *
     * @return float
     */
    public static function moneyForHumans(int $balance): float
    {
        return $balance / 100;
    }

    /**
     * @param float $balance
     *
     * @return int
     */
    public static function moneyForEntity(float $balance): int
    {
        return round($balance * 100);
    }

}