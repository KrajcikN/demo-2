<?php

namespace app\application\services;


use app\application\dto\EmployeeSalaryDto;
use app\application\entities\date\Month;
use app\application\entities\Employee;

class SalaryCalculationService
{
    /**
     * @var BonusService
     */
    private $bonusService;

    /**
     * SalaryCalculationService constructor.
     *
     * @param BonusService $bonusService
     */
    public function __construct(BonusService $bonusService)
    {
        $this->bonusService = $bonusService;
    }

    /**
     * @param Employee $employee
     * @param Month    $month
     *
     * @return EmployeeSalaryDto
     * @throws \DomainException
     */
    public function calculateMonthlyByEmployee(Employee $employee, Month $month): EmployeeSalaryDto
    {
        $bonuses = $this->bonusService->getMonthlyByEmployee($employee, $month);
        $sum = 0;
        foreach ($bonuses as $bonus) {
            $sum += array_sum($bonus);
        }
        $employeeSalary = $employee->fixed_salary + $sum;

        return new EmployeeSalaryDto(
            $employee->id,
            $employee->name,
            $employeeSalary,
            $month
        );
    }
}