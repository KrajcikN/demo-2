<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bonuses`.
 */
class m171023_103855_create_bonuses_table extends Migration
{
    public $table = '{{%bonuses}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id'     => $this->primaryKey(),
                'name'   => $this->string()->unique()->notNull(),
                'min'    => $this->integer(),
                'max'    => $this->integer(),
                'cost'   => $this->integer()->notNull(),
                'active' => $this->smallInteger()->defaultValue(0),
            ]
        );

        $this->batchInsert(
            $this->table,
            ['name', 'min', 'max', 'cost', 'active'],
            [
                ['Начальный', 0, 100, 10000, 1],
                ['Средний', 101, 200, 20000, 1],
                ['Высший', 301, null, 30000, 1],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
