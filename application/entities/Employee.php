<?php

namespace app\application\entities;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%employee}}".
 *
 * @property integer $id
 * @property string  $name
 * @property integer $fixed_salary
 */
class Employee extends ActiveRecord
{
    public $bonuses = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employee}}';
    }
}
