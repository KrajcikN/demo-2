<?php

/* @var $this yii\web\View */

use app\application\helpers\MoneyHelper;
use yii\helpers\Html;

/* @var $bonuses \app\application\dto\EmployeeBonusHistoryDto */

$this->title = 'Bonus history';
?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="employees-index">
    <p><?= Html::encode($bonuses->getName()) ?></p>
    <ul>
        <?php foreach ($bonuses->getBonuses() as $date => $bonus) : ?>
            <li><?= $date.':  '.Yii::$app->formatter->asCurrency(
                    MoneyHelper::moneyForHumans(
                        array_sum($bonus)
                    )
                ) ?></li>
        <?php endforeach; ?>
    </ul>
</div>
