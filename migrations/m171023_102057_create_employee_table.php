<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m171023_102057_create_employee_table extends Migration
{
    public $table = '{{%employee}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id'           => $this->primaryKey(),
                'name'         => $this->string()->notNull()->unique(),
                'fixed_salary' => $this->integer(),
            ]
        );
        $this->batchInsert(
            $this->table,
            ['id', 'name', 'fixed_salary'],
            [
                [1, 'Хельга Браун', 2000000],
                [2, 'Барак Обама', 3000000],
                [3, 'Денис Козлов', 4000000],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
